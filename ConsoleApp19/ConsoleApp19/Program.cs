﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp19
{
    internal class Program
    {
        static void Main(string[] args)
        {

            List<Pass> passes = new List<Pass>();
            List<Student> students = new List<Student>();
            List<Instructor> instructors = new List<Instructor>();
            List<Course> courses = new List<Course>();
            List<Student> chosenStudents = new List<Student>();

            int credits = int.Parse(Console.ReadLine());

            string connetionString;
            SqlConnection cnn;
            connetionString = @"Server=DESKTOP-ROF30FK;Database=Coursera;Trusted_Connection=True";
            cnn = new SqlConnection(connetionString);

            cnn.Open();

            SqlCommand command;
            SqlDataReader dataReader;
            String sql, Output = "";

           



            //Instructors
            sql = "Select id,first_name,last_name from instructors";

            command = new SqlCommand(sql, cnn);

            dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Instructor instructor = new Instructor();
                instructor.Id = dataReader.GetValue(0);
                instructor.FirstName = dataReader.GetValue(1);
                instructor.LastName = dataReader.GetValue(2);
                instructors.Add(instructor);

            }

            dataReader.Close();
            command.Dispose();



            //Courses
            sql = "Select id,name,instructor_id,total_time,credit from courses";

            command = new SqlCommand(sql, cnn);

            dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Course course = new Course();
                course.Id = dataReader.GetValue(0);
                course.Name = dataReader.GetValue(1);
                course.Instructor = instructors.Where(i => i.Id.Equals(dataReader.GetValue(2)) ).FirstOrDefault();
                course.TotalTime = dataReader.GetValue(3);
                course.Credit = dataReader.GetValue(4);

                courses.Add(course);

            }

            dataReader.Close();
            command.Dispose();

            //Students
            sql = "Select pin,first_name,last_name from students";

            command = new SqlCommand(sql, cnn);

            dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Student student = new Student();
                student.Pin = dataReader.GetValue(0);
                student.FirstName = dataReader.GetValue(1);
                student.LastName = dataReader.GetValue(2);
                students.Add(student);

            }

            dataReader.Close();
            command.Dispose();



            //Pass
            sql = "Select student_pin,course_id from students_courses_xref";

            command = new SqlCommand(sql, cnn);

            dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Pass pass = new Pass();
                pass.Student = students.Where(i => i.Pin.Equals(dataReader.GetValue(0))).FirstOrDefault();
                pass.Course = courses.Where(i => i.Id.Equals(dataReader.GetValue(1))).FirstOrDefault();
                passes.Add(pass);

            }

            dataReader.Close();
            command.Dispose();



            cnn.Close();


            foreach(var st in students)
            {
                List<Pass> passList = new List<Pass>();
                st.TotalCredits = (int)0;

                foreach (var p in passes)
                {
                    if (st.Pin.Equals(p.Student.Pin))
                    {
                        passList.Add(p);
                        st.TotalCredits += Convert.ToInt32(p.Course.Credit);
                    }

                }
                st.PassedCourses = passList;
            }

            chosenStudents = students.Where(s => s.TotalCredits >= credits).ToList();

            string html = "<!DOCTYPE html>\r\n<html lang=\"en\">\r\n\r\n  <head>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\r\n    <meta name=\"description\" content=\"\">\r\n    <meta name=\"author\" content=\"\">\r\n     <title>Reports</title>\r\n\r\n  <style>\r\ntable, th, td {\r\n  border:1px solid black;\r\n}\r\n</style>  </head>";
            html += "<table>";
            html += "<tr>";
            html += "<th> Student </th>  <th> Total Credits </th>";
            html += "</tr>";
            html += "<tr>";
            html += "<th> Course Name </th >  <th> Time </th> <th> Credit </th> <th> Instructor </th>";
            html += "</tr>";

            foreach (var c in chosenStudents)
            {
                html += "<tr>";
                html += $"<td> {c.FirstName} {c.LastName} </td>";
                html += $"<td> {c.TotalCredits} </td>";
                html += "</tr>";

                foreach(var pc in c.PassedCourses)
                {
                    html += "<tr>";
                    html += $"<td> {pc.Course.Name} </td>";
                    html += $"<td> {pc.Course.TotalTime} </td>";
                    html += $"<td> {pc.Course.Credit} </td>";
                    html += $"<td> {pc.Course.Instructor.FirstName} {pc.Course.Instructor.LastName} </td>";
                    html += $"<tr>";

                }

            }

            html += "<table>";
            html += "</body> </html>";

            Console.WriteLine(html);

            File.WriteAllText(@"C:\Users\zhnin\Desktop\report.html", html);
        }
    }
}

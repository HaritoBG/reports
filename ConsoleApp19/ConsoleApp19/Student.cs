﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp19
{
    internal class Student
    {

        public object Pin { get; set; }

        public object FirstName { get; set; }

        public object LastName { get; set; }

        public List<Pass> PassedCourses { get; set; }

        public int TotalCredits { get; set; }

       
    }
}

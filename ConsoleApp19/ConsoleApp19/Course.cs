﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp19
{
    internal class Course
    {

        public object Id { get; set; }

        public object Name { get; set; }

        public Instructor Instructor { get; set; }

        public object TotalTime { get; set; }

        public object Credit { get; set; }


    }
}
